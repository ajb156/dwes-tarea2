<?php

// Guardar un registro en el archivo.
function save($name, $phone)
{
  $file = "datos.txt";
  $fp = fopen($file, "a+") or die("Error al leer el archivo");
  fwrite($fp, $name);
  fwrite($fp, ' ');
  fwrite($fp, $phone);
  fwrite($fp, "\n\n");
  fclose($fp);
}

function search($name)
{
  $file = "datos.txt";
  $fp = fopen($file, "r") or die("Error al leer el archivo");
  $matches = array();
  if ($fp)
  {
      while (!feof($fp))
      {
          $buffer = fgets($fp);
          if(strpos($buffer, $name) !== FALSE){
            $matches[] = $buffer;
          }
      }
      fclose($fp);
  }
  
  if (count($matches) > 0) {
    return true;
  }else{
    return false;
  }
}


// Funcion para borrar un archivo
function delete()
{
  $file = "datos.txt";
  unlink($file);
}
