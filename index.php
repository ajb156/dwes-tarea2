<?php
include_once './partials/header.php';
?>

<h1>Agenda de contactos.</h1>

<?php 


if (isset($_POST['guardar'])) {
  $name = $_POST['name'];
  $phone = $_POST['phone'];
}

?>

<form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']) ?>" method="post" name="form">
  <label for="name">Nombre:</label>
  
  <input type="text" name="name" value="<?php if(isset($name)) echo $name ?>">

  <label for="phone">Teléfono:</label>
  <input type="number" name="phone" value="<?php if(isset($phone)) echo $phone ?>">

  <input type="submit" value="Guardar" name="guardar">

  <?php

  include './validator.php';

  ?>

</form>

<h2>Listado de contactos.</h2>
<hr>

<?php
  include_once 'list.php';
?>

<?php
include_once './partials/footer.php';
?>